<?php

namespace Sng\Test\Service;

use PHPUnit\Framework\TestCase;
use Sng\Client;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

class AccountTest extends TestCase
{
    public function testBuilder()
    {
        $client = new Client(['apex' => 'local.sng.com:8080']);
        $accountService = $client->Account();

        $this->assertInstanceOf('Sng\\Service\\Account', $accountService);
    }

    public function testCreateAccountExisting()
    {
        $client = new Client(['apex' => 'local.sng.com:8080']);

        $mock = new MockHandler([
            new Response(409),
        ]);
        $handler = HandlerStack::create($mock);
        $mockClient = new GuzzleClient(['handler' => $handler]);
        $client->setHttpClient($mockClient);

        $this->expectException(\Sng\Exception\AlreadyExistsException::class);
        $client->Account()->createAccount('foo@bar.com', 'pass');
    }

    public function testCreateAccountInvalidParams()
    {
        $client = new Client(['apex' => 'local.sng.com:8080']);

        $mock = new MockHandler([
            new Response(400),
        ]);
        $handler = HandlerStack::create($mock);
        $mockClient = new GuzzleClient(['handler' => $handler]);
        $client->setHttpClient($mockClient);

        $this->expectException(\InvalidArgumentException::class);
        $client->Account()->createAccount('foo@bar.com', 'pass');
    }

    public function testCreateAccountSuccessful()
    {
        $client = new Client(['apex' => 'local.sng.com:8080']);

        $mock = new MockHandler([
            new Response(200, [], json_encode(['foo' => 'bar'])),
        ]);
        $handler = HandlerStack::create($mock);
        $mockClient = new GuzzleClient(['handler' => $handler]);
        $client->setHttpClient($mockClient);

        $data = $client->Account()->createAccount('foo@bar.com', 'pass');

        $this->assertArrayHasKey('foo', $data);
        $this->assertEquals('bar', $data['foo']);
    }

    public function testCreateAccountInvalidJson()
    {
        $client = new Client(['apex' => 'local.sng.com:8080']);

        $mock = new MockHandler([
            new Response(200, [], 'foo'),
        ]);
        $handler = HandlerStack::create($mock);
        $mockClient = new GuzzleClient(['handler' => $handler]);
        $client->setHttpClient($mockClient);

        $this->expectException(\UnexpectedValueException::class);
        $client->Account()->createAccount('foo@bar.com', 'pass');
    }

    public function testCreateAccountUnknownResponse()
    {
        $client = new Client(['apex' => 'local.sng.com:8080']);

        $mock = new MockHandler([
            new Response(500),
        ]);
        $handler = HandlerStack::create($mock);
        $mockClient = new GuzzleClient(['handler' => $handler]);
        $client->setHttpClient($mockClient);

        $this->expectException(\UnexpectedValueException::class);
        $client->Account()->createAccount('foo@bar.com', 'pass');
    }
}
