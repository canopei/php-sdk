<?php

namespace Sng\Test\Service;

use PHPUnit\Framework\TestCase;
use Sng\Client;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

class Siteest extends TestCase
{
    public function testBuilder()
    {
        $client = new Client(['apex' => 'local.sng.com:8080']);
        $siteService = $client->Site();

        $this->assertInstanceOf('Sng\\Service\\Site', $siteService);
    }

    public function testGetSitesServerError()
    {
        $client = new Client(['apex' => 'local.sng.com:8080']);

        $mock = new MockHandler([
            new Response(500),
        ]);
        $handler = HandlerStack::create($mock);
        $mockClient = new GuzzleClient(['handler' => $handler]);
        $client->setHttpClient($mockClient);

        $this->expectException(\UnexpectedValueException::class);
        $client->Site()->getSites(10, 1);
    }

    public function testGetSitesInvalidJson()
    {
        $client = new Client(['apex' => 'local.sng.com:8080']);

        $mock = new MockHandler([
            new Response(200, [], 'foo'),
        ]);
        $handler = HandlerStack::create($mock);
        $mockClient = new GuzzleClient(['handler' => $handler]);
        $client->setHttpClient($mockClient);

        $this->expectException(\UnexpectedValueException::class);
        $client->Site()->getSites(10, 1);
    }

    public function testGetSitesSuccessful()
    {
        $client = new Client(['apex' => 'local.sng.com:8080']);

        $mock = new MockHandler([
            new Response(200, [], json_encode(['sites' => [
                [
                    "uuid" => "53ba5bf0-5204-4ccb-48b5-dda7d6ddd151",
                    "name" => "foo",
                    "mbId" => -99,
                    "createdAt" => "2017-05-11 14:07:43",
                ],
                [
                    "uuid" => "12oa5bf0-5204-4ccb-48b5-dda7d6ddd151",
                    "name" => "bar",
                    "mbId" => -99,
                    "createdAt" => "2017-05-11 14:07:44",
                ]
            ]]))
        ]);
        $handler = HandlerStack::create($mock);
        $mockClient = new GuzzleClient(['handler' => $handler]);
        $client->setHttpClient($mockClient);

        $data = $client->Site()->getSites(10, 1);

        $this->assertInternalType('array', $data);
        $this->assertArrayHasKey('sites', $data);
        $this->assertCount(2, $data['sites']);
        $this->assertEquals('bar', $data['sites'][1]['name']);
    }
}