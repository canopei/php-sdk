<?php

namespace Sng\Test;

use PHPUnit\Framework\TestCase;
use Sng\Client;
use Sng\Service\Account;

class ClientTest extends TestCase
{
    public function testConstructor()
    {
        // also check the PROXY env
        putenv("HTTP_PROXY=bar");

        $client = new Client([
            'apex' => 'foo',
        ]);

        // unset it
        putenv("HTTP_PROXY");

        // quickly test these
        $client->setHttpClient($client->getHttpClient());

        $this->assertInstanceOf(Client::class, $client);
        $this->assertEquals('foo', $client->getApex());
        $this->assertEquals(Client::VERSION, $client->getVersion());
    }

    public function testMagicCall()
    {
        $client = new Client();

        // It should work with an existing service
        $service = $client->Account();
        $this->assertInstanceOf(Account::class, $service);

        // We should get the same instance from the array cache
        $sameService = $client->Account();
        $this->assertSame($service, $sameService);

        // Should throw exception for non-existing method
        $this->expectException(\BadMethodCallException::class);
        $client->Foo();
    }
}