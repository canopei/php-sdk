<?php

namespace Sng\Model\Sale;

class Purchase implements \JsonSerializable
{
    private $uuid;
    private $accountId;
    private $referenceId;
    private $firstName;
    private $lastName;
    private $country;
    private $address;
    private $address2;
    private $city;
    private $state;
    private $zip;
    private $total;
    private $tax;
    private $products;

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     * @return Purchase
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @param mixed $accountId
     * @return Purchase
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReferenceId()
    {
        return $this->referenceId;
    }

    /**
     * @param mixed $referenceId
     * @return Purchase
     */
    public function setReferenceId($referenceId)
    {
        $this->referenceId = $referenceId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     * @return Purchase
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     * @return Purchase
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     * @return Purchase
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return Purchase
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @param mixed $address2
     * @return Purchase
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return Purchase
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     * @return Purchase
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param mixed $zip
     * @return Purchase
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     * @return Purchase
     */
    public function setTotal($total)
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param mixed $tax
     * @return Purchase
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     * @return Purchase
     */
    public function setProducts($products)
    {
        $this->products = $products;
        return $this;
    }

    public static function fromApi($data)
    {
        $purchase = new self();
        $purchase
            ->setUuid($data['uuid'])
            ->setFirstName($data['firstName'])
            ->setLastName($data['lastName'])
            ->setAccountId($data['accountId'])
            ->setReferenceId($data['referenceId'])
            ->setCountry($data['country'])
            ->setAddress($data['address'])
            ->setAddress2($data['address2'])
            ->setCity($data['city'])
            ->setState($data['state'])
            ->setZip($data['zip'])
            ->setTotal($data['total'])
            ->setTax($data['tax'])
            ->setProducts($data['products'])
        ;

        return $purchase;
    }

    public function jsonSerialize()
    {
        return [
            'uuid' => $this->getUuid(),
            'accountId' => $this->getAccountId(),
            'referenceId' => $this->getReferenceId(),
            'firstName' => $this->getFirstName(),
            'lastName' => $this->getLastName(),
            'country' => $this->getCountry(),
            'address' => $this->getAddress(),
            'address2' => $this->getAddress2(),
            'city' => $this->getCity(),
            'state' => $this->getState(),
            'zip' => $this->getZip(),
            'total' => $this->getTotal(),
            'tax' => $this->getTax(),
            'products' => $this->getProducts(),
        ];
    }
}