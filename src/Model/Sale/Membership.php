<?php

namespace Sng\Model\Sale;

class Membership implements \JsonSerializable
{
    private $uuid;
    private $name;
    private $description;
    private $count;
    private $validForDays;
    private $price;
    private $tax;
    private $isFeatured;
    private $isAvailable;

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     * @return Membership
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Membership
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Membership
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     * @return Membership
     */
    public function setCount($count)
    {
        $this->count = $count;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValidForDays()
    {
        return $this->validForDays;
    }

    /**
     * @param mixed $validForDays
     * @return Membership
     */
    public function setValidForDays($validForDays)
    {
        $this->validForDays = $validForDays;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return Membership
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param mixed $tax
     * @return Membership
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsFeatured()
    {
        return $this->isFeatured;
    }

    /**
     * @param mixed $isFeatured
     * @return Membership
     */
    public function setIsFeatured($isFeatured)
    {
        $this->isFeatured = $isFeatured;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsAvailable()
    {
        return $this->isAvailable;
    }

    /**
     * @param mixed $isAvailable
     * @return Membership
     */
    public function setIsAvailable($isAvailable)
    {
        $this->isAvailable = $isAvailable;
        return $this;
    }

    public static function fromApi($data)
    {
        $membership = new self();
        $membership
            ->setUuid($data['uuid'])
            ->setName($data['name'])
            ->setDescription($data['description'])
            ->setCount($data['count'])
            ->setValidForDays($data['validForDays'])
            ->setPrice($data['price'])
            ->setTax($data['tax'])
            ->setIsAvailable($data['isAvailable'])
            ->setIsFeatured($data['isFeatured'])
        ;

        return $membership;
    }

    public function jsonSerialize()
    {
        return [
            'uuid' => $this->getUuid(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'count' => $this->getCount(),
            'validForDays' => $this->getValidForDays(),
            'price' => $this->getPrice(),
            'tax' => $this->getTax(),
            'isFeatured' => $this->getIsFeatured(),
            'isAvailable' => $this->getIsAvailable(),
        ];
    }
}