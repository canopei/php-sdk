<?php

namespace Sng\Model\Sale;

class Service  implements \JsonSerializable
{
    private $mbId;
    private $siteUuid;
    private $name;
    private $price;
    private $taxIncluded;
    private $taxRate;
    private $count;

    private $siteName;

    /**
     * @return mixed
     */
    public function getMbId()
    {
        return $this->mbId;
    }

    /**
     * @param mixed $mbId
     * @return Service
     */
    public function setMbId($mbId)
    {
        $this->mbId = $mbId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteUuid()
    {
        return $this->siteUuid;
    }

    /**
     * @param mixed $siteUuid
     * @return Service
     */
    public function setSiteUuid($siteUuid)
    {
        $this->siteUuid = $siteUuid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteName()
    {
        return $this->siteName;
    }

    /**
     * @param mixed $siteName
     * @return Service
     */
    public function setSiteName($siteName)
    {
        $this->siteName = $siteName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Service
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return Service
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTaxIncluded()
    {
        return $this->taxIncluded;
    }

    /**
     * @param mixed $taxIncluded
     * @return Service
     */
    public function setTaxIncluded($taxIncluded)
    {
        $this->taxIncluded = $taxIncluded;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTaxRate()
    {
        return $this->taxRate;
    }

    /**
     * @param mixed $taxRate
     * @return Service
     */
    public function setTaxRate($taxRate)
    {
        $this->taxRate = $taxRate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     * @return Service
     */
    public function setCount($count)
    {
        $this->count = $count;
        return $this;
    }

    public static function fromApi($data)
    {
        $service = new self();
        $service
            ->setMbId($data['mbId'])
            ->setSiteUuid(isset($data['siteUuid']) ? $data['siteUuid'] : '')
            ->setName($data['name'])
            ->setPrice($data['price'])
            ->setCount($data['count'])
            ->setTaxRate($data['taxRate'])
            ->setTaxIncluded($data['taxIncluded'])
        ;

        return $service;
    }

    public function jsonSerialize()
    {
        return [
            'mbId' => $this->getMbId(),
            'siteUuid' => $this->getSiteUuid(),
            'name' => $this->getName(),
            'count' => $this->getCount(),
            'taxRate' => $this->getTaxRate(),
            'taxIncluded' => $this->getTaxIncluded(),
        ];
    }
}