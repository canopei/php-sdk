<?php

namespace Sng\Model\Account;

use Sng\Model\Site\Site;

class ClientService implements \JsonSerializable
{
    private $mbId;
    private $name;
    private $siteName;
    private $paymentDate;
    private $activeDate;
    private $expirationDate;
    private $current;
    private $count;
    private $remaining;

    /**
     * @return mixed
     */
    public function getMbId()
    {
        return $this->mbId;
    }

    /**
     * @param mixed $mbId
     */
    public function setMbId($mbId)
    {
        $this->mbId = $mbId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentDate()
    {
        return $this->paymentDate;
    }

    /**
     * @param mixed $paymentDate
     */
    public function setPaymentDate($paymentDate)
    {
        $this->paymentDate = $paymentDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getActiveDate()
    {
        return $this->activeDate;
    }

    /**
     * @param mixed $activeDate
     */
    public function setActiveDate($activeDate)
    {
        $this->activeDate = $activeDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * @param mixed $expirationDate
     */
    public function setExpirationDate($expirationDate)
    {
        $this->expirationDate = $expirationDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * @param mixed $current
     */
    public function setCurrent($current)
    {
        $this->current = $current;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count)
    {
        $this->count = $count;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRemaining()
    {
        return $this->remaining;
    }

    /**
     * @param mixed $remaining
     */
    public function setRemaining($remaining)
    {
        $this->remaining = $remaining;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteName()
    {
        return $this->siteName;
    }

    /**
     * @param mixed $siteName
     * @return ClientService
     */
    public function setSiteName($siteName)
    {
        $this->siteName = $siteName;
        return $this;
    }

    public static function fromApi($data)
    {
        $service = new self();
        $service
            ->setMbId($data['mbId'])
            ->setName($data['name'])
            ->setSiteName($data['siteName'])
            ->setPaymentDate($data['paymentDate'])
            ->setActiveDate($data['activeDate'])
            ->setExpirationDate($data['expirationDate'])
            ->setCurrent($data['current'])
            ->setCount($data['count'])
            ->setRemaining($data['remaining'])
        ;

        return $service;
    }

    public function jsonSerialize()
    {
        return [
            'mbId' => $this->getMbId(),
            'name' => $this->getName(),
            'siteName' => $this->getSiteName(),
            'paymentDate' => $this->getPaymentDate(),
            'activeDate' => $this->getActiveDate(),
            'expirationDate' => $this->getExpirationDate(),
            'current' => $this->getCurrent(),
            'count' => $this->getCount(),
            'remaining' => $this->getRemaining(),
        ];
    }
}