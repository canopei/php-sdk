<?php

namespace Sng\Model\Account;

use Sng\Model\Site\Site;

class Client implements \JsonSerializable
{
    private $uuid;
    private $siteId;
    private $mbId;
    private $firstName;
    private $middleName;
    private $lastName;
    private $gender;
    private $email;
    private $emailOptin;
    private $city;
    private $country;
    private $state;
    private $status;
    private $username;
    private $isCompany;
    private $inactive;
    private $createdAt;
    private $modifiedAt;

    private $site;

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteId()
    {
        return $this->siteId;
    }

    /**
     * @param mixed $siteId
     */
    public function setSiteId($siteId)
    {
        $this->siteId = $siteId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMbId()
    {
        return $this->mbId;
    }

    /**
     * @param mixed $mbId
     */
    public function setMbId($mbId)
    {
        $this->mbId = $mbId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param mixed $middleName
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmailOptin()
    {
        return $this->emailOptin;
    }

    /**
     * @param mixed $emailOptin
     */
    public function setEmailOptin($emailOptin)
    {
        $this->emailOptin = $emailOptin;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getisCompany()
    {
        return $this->isCompany;
    }

    /**
     * @param mixed $isCompany
     */
    public function setIsCompany($isCompany)
    {
        $this->isCompany = $isCompany;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInactive()
    {
        return $this->inactive;
    }

    /**
     * @param mixed $inactive
     */
    public function setInactive($inactive)
    {
        $this->inactive = $inactive;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param mixed $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param mixed $site
     */
    public function setSite(Site $site)
    {
        $this->site = $site;
        return $this;
    }

    public static function fromApi($data)
    {
        $client = new self();
        $client
            ->setUuid($data['uuid'])
            ->setSiteId($data['siteId'])
            ->setMbId($data['mbId'])
            ->setFirstName($data['firstName'])
            ->setMiddleName($data['middleName'])
            ->setLastName($data['lastName'])
            ->setGender($data['gender'])
            ->setEmail($data['email'])
            ->setEmailOptin($data['emailOptin'])
            ->setCity($data['city'])
            ->setCountry($data['country'])
            ->setState($data['state'])
            ->setStatus($data['status'])
            ->setUsername($data['username'])
            ->setIsCompany($data['isCompany'])
            ->setInactive($data['inactive'])
            ->setCreatedAt($data['createdAt'])
            ->setModifiedAt($data['modifiedAt'])
        ;

        return $client;
    }

    public function jsonSerialize()
    {
        return [
            'uuid' => $this->getUuid(),
            'siteId' => $this->getSiteId(),
            'mbId' => $this->getMbId(),
            'firstName' => $this->getFirstName(),
            'middleName' => $this->getMiddleName(),
            'lastName' => $this->getLastName(),
            'gender' => $this->getGender(),
            'email' => $this->getEmail(),
            'emailOptin' => $this->getEmailOptin(),
            'city' => $this->getCity(),
            'country' => $this->getCountry(),
            'state' => $this->getState(),
            'status' => $this->getStatus(),
            'username' => $this->getUsername(),
            'isCompany' => $this->getisCompany(),
            'inactive' => $this->getInactive(),
        ];
    }
}