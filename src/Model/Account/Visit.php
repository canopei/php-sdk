<?php

namespace Sng\Model\Account;

class Visit
{
    private $uuid;
    private $classUuid;
    private $className;
    private $levelName;
    private $startDatetime;
    private $endDatetime;
    private $locationName;
    private $lateCancelInterval;
    private $staffFirstName;
    private $staffLastName;
    private $hideCancel;

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     * @return Visit
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * @param mixed $className
     * @return Visit
     */
    public function setClassName($className)
    {
        $this->className = $className;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClassUuid()
    {
        return $this->classUuid;
    }

    /**
     * @param mixed $classUuid
     * @return Visit
     */
    public function setClassUuid($classUuid)
    {
        $this->classUuid = $classUuid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLevelName()
    {
        return $this->levelName;
    }

    /**
     * @param mixed $levelName
     * @return Visit
     */
    public function setLevelName($levelName)
    {
        $this->levelName = $levelName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartDatetime()
    {
        return $this->startDatetime;
    }

    /**
     * @param mixed $startDatetime
     * @return Visit
     */
    public function setStartDatetime($startDatetime)
    {
        $this->startDatetime = $startDatetime;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndDatetime()
    {
        return $this->endDatetime;
    }

    /**
     * @param mixed $endDatetime
     * @return Visit
     */
    public function setEndDatetime($endDatetime)
    {
        $this->endDatetime = $endDatetime;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocationName()
    {
        return $this->locationName;
    }

    /**
     * @param mixed $locationName
     * @return Visit
     */
    public function setLocationName($locationName)
    {
        $this->locationName = $locationName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStaffFirstName()
    {
        return $this->staffFirstName;
    }

    /**
     * @param mixed $staffFirstName
     * @return Visit
     */
    public function setStaffFirstName($staffFirstName)
    {
        $this->staffFirstName = $staffFirstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStaffLastName()
    {
        return $this->staffLastName;
    }

    /**
     * @param mixed $staffLastName
     * @return Visit
     */
    public function setStaffLastName($staffLastName)
    {
        $this->staffLastName = $staffLastName;
        return $this;
    }

    public function getDuration()
    {
        $start = strtotime($this->getStartDatetime());
        $end = strtotime($this->getEndDatetime());

        return $end - $start;
    }

    /**
     * @return mixed
     */
    public function getHideCancel()
    {
        return $this->hideCancel;
    }

    /**
     * @param mixed $hideCancel
     * @return Visit
     */
    public function setHideCancel($hideCancel)
    {
        $this->hideCancel = $hideCancel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLateCancelInterval()
    {
        return $this->lateCancelInterval;
    }

    /**
     * @param mixed $lateCancelInterval
     * @return Visit
     */
    public function setLateCancelInterval($lateCancelInterval)
    {
        $this->lateCancelInterval = $lateCancelInterval;
        return $this;
    }

    public function isLateCancel()
    {
        return ((strtotime($this->getStartDatetime()) - time()) / 60) < $this->getLateCancelInterval();
    }

    public static function fromApi($data)
    {
        $visit = new self();
        $visit
            ->setUuid(isset($data['uuid']) ? $data['uuid'] : '')
            ->setClassUuid($data['classUuid'])
            ->setClassName($data['className'])
            ->setLevelName($data['levelName'])
            ->setStartDatetime($data['startDatetime'])
            ->setEndDatetime($data['endDatetime'])
            ->setStaffFirstName($data['staffFirstName'])
            ->setStaffLastName($data['staffLastName'])
            ->setLocationName($data['locationName'])
            ->setLateCancelInterval($data['lateCancelInterval'])
            ->setHideCancel($data['hideCancel'])
        ;

        return $visit;
    }
}