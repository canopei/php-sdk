<?php

namespace Sng\Model\Account;

class Account
{
    private $uuid;
    private $email;
    private $fullName;
    private $gender;
    private $profileImageUrl;
    private $confirmationCode;
    private $confirmedAt;
    private $createdAt;
    private $modifiedAt;

    private $roles;

    public function __construct()
    {
        $this->roles = array();
    }

    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getFullName()
    {
        return $this->fullName;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setProfileImageUrl($profileImageUrl)
    {
        $this->profileImageUrl = $profileImageUrl;

        return $this;
    }

    public function getProfileImageUrl()
    {
        return $this->profileImageUrl;
    }

    public function setConfirmationCode($confirmationCode)
    {
        $this->confirmationCode = $confirmationCode;

        return $this;
    }

    public function getConfirmationCode()
    {
        return $this->confirmationCode;
    }

    public function setConfirmedAt($confirmedAt)
    {
        $this->confirmedAt = $confirmedAt;

        return $this;
    }

    public function getConfirmedAt()
    {
        return $this->confirmedAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    public static function fromApi($data)
    {
        $account = new self();
        $account
            ->setUuid($data['uuid'])
            ->setEmail($data['email'])
            ->setFullName($data['fullName'])
            ->setGender($data['gender'])
            ->setProfileImageUrl($data['profileImageUrl'])
            ->setConfirmationCode($data['confirmationCode'])
            ->setConfirmedAt($data['confirmedAt'])
            ->setCreatedAt($data['createdAt'])
            ->setModifiedAt($data['modifiedAt'])
            ;

        return $account;
    }
}
