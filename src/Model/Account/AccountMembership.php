<?php

namespace Sng\Model\Account;

use Sng\Model\Sale\Membership;

class AccountMembership implements \JsonSerializable
{
    private $id;
    private $accountId;
    private $membershipId;
    private $remaining;
    private $validUntil;
    private $createdAt;

    private $membership;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return AccountMembership
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @param mixed $accountId
     * @return AccountMembership
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMembershipId()
    {
        return $this->membershipId;
    }

    /**
     * @param mixed $membershipId
     * @return AccountMembership
     */
    public function setMembershipId($membershipId)
    {
        $this->membershipId = $membershipId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRemaining()
    {
        return $this->remaining;
    }

    /**
     * @param mixed $remaining
     * @return AccountMembership
     */
    public function setRemaining($remaining)
    {
        $this->remaining = $remaining;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValidUntil()
    {
        return $this->validUntil;
    }

    /**
     * @param mixed $validUntil
     * @return AccountMembership
     */
    public function setValidUntil($validUntil)
    {
        $this->validUntil = $validUntil;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return AccountMembership
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMembership()
    {
        return $this->membership;
    }

    /**
     * @param mixed $membership
     * @return AccountMembership
     */
    public function setMembership(Membership $membership)
    {
        $this->membership = $membership;
        return $this;
    }

    public function getRemainingDays()
    {
        return ceil((strtotime($this->getValidUntil()) - time()) / 86400);
    }

    public static function fromApi($data)
    {
        $memb = new self();
        $memb->setId($data['id'])
            ->setAccountId($data['accountId'])
            ->setMembershipId($data['membershipId'])
            ->setRemaining($data['remaining'])
            ->setValidUntil($data['validUntil'])
            ->setCreatedAt($data['createdAt'])
        ;

        return $memb;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'accountId' => $this->getAccountId(),
            'membershipId' => $this->getMembershipId(),
            'remaining' => $this->getRemaining(),
            'validUntil' => $this->getValidUntil(),
            'createdAt' => $this->getCreatedAt(),
            'membership' => $this->getMembership()
        ];
    }
}
