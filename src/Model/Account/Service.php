<?php

namespace Sng\Model\Account;

class Service
{
    private $mbId;
    private $prograMbId;
    private $name;
    private $siteName;
    private $current;
    private $count;
    private $remaining;
    private $paymentDate;
    private $activeDate;
    private $expirationDate;

    /**
     * @return mixed
     */
    public function getMbId()
    {
        return $this->mbId;
    }

    /**
     * @param mixed $mbId
     * @return Service
     */
    public function setMbId($mbId)
    {
        $this->mbId = $mbId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrograMbId()
    {
        return $this->prograMbId;
    }

    /**
     * @param mixed $prograMbId
     * @return Service
     */
    public function setPrograMbId($prograMbId)
    {
        $this->prograMbId = $prograMbId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Service
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteName()
    {
        return $this->siteName;
    }

    /**
     * @param mixed $siteName
     * @return Service
     */
    public function setSiteName($siteName)
    {
        $this->siteName = $siteName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * @param mixed $current
     * @return Service
     */
    public function setCurrent($current)
    {
        $this->current = $current;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     * @return Service
     */
    public function setCount($count)
    {
        $this->count = $count;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRemaining()
    {
        return $this->remaining;
    }

    /**
     * @param mixed $remaining
     * @return Service
     */
    public function setRemaining($remaining)
    {
        $this->remaining = $remaining;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentDate()
    {
        return $this->paymentDate;
    }

    /**
     * @param mixed $paymentDate
     * @return Service
     */
    public function setPaymentDate($paymentDate)
    {
        $this->paymentDate = $paymentDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getActiveDate()
    {
        return $this->activeDate;
    }

    /**
     * @param mixed $activeDate
     * @return Service
     */
    public function setActiveDate($activeDate)
    {
        $this->activeDate = $activeDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * @param mixed $expirationDate
     * @return Service
     */
    public function setExpirationDate($expirationDate)
    {
        $this->expirationDate = $expirationDate;
        return $this;
    }

    public function getRemainingDays()
    {
        return ceil((strtotime($this->getExpirationDate()) - time()) / 86400);
    }

    public static function fromApi($data)
    {
        $service = new self();
        $service
            ->setMbId($data['mbId'])
            ->setPrograMbId($data['programMbId'])
            ->setName($data['name'])
            ->setSiteName($data['siteName'])
            ->setCurrent($data['current'])
            ->setCount($data['count'])
            ->setRemaining($data['remaining'])
            ->setPaymentDate($data['paymentDate'])
            ->setActiveDate($data['activeDate'])
            ->setExpirationDate($data['expirationDate'])
        ;

        return $service;
    }
}