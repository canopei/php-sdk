<?php

namespace Sng\Model\ScheduledClass;

class Booking implements \JsonSerializable
{
    private $id;
    private $accountId;
    private $classId;
    private $siteId;
    private $locationId;
    private $accountMembershipId;
    private $membershipServiceId;
    private $serviceId;
    private $status;
    private $createdAt;
    private $modifiedAt;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Booking
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @param mixed $accountId
     * @return Booking
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClassId()
    {
        return $this->classId;
    }

    /**
     * @param mixed $classId
     * @return Booking
     */
    public function setClassId($classId)
    {
        $this->classId = $classId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteId()
    {
        return $this->siteId;
    }

    /**
     * @param mixed $siteId
     * @return Booking
     */
    public function setSiteId($siteId)
    {
        $this->siteId = $siteId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * @param mixed $locationId
     * @return Booking
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAccountMembershipId()
    {
        return $this->accountMembershipId;
    }

    /**
     * @param mixed $accountMembershipId
     * @return Booking
     */
    public function setAccountMembershipId($accountMembershipId)
    {
        $this->accountMembershipId = $accountMembershipId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMembershipServiceId()
    {
        return $this->membershipServiceId;
    }

    /**
     * @param mixed $membershipServiceId
     * @return Booking
     */
    public function setMembershipServiceId($membershipServiceId)
    {
        $this->membershipServiceId = $membershipServiceId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * @param mixed $serviceId
     * @return Booking
     */
    public function setServiceId($serviceId)
    {
        $this->serviceId = $serviceId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Booking
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return Booking
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param mixed $modifiedAt
     * @return Booking
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
        return $this;
    }

    public static function fromApi($data)
    {
        $booking = new self();
        $booking
            ->setId($data['id'])
            ->setAccountId($data['accountId'])
            ->setClassId($data['classId'])
            ->setSiteId($data['siteId'])
            ->setLocationId($data['locationId'])
            ->setMembershipServiceId($data['membershipServiceId'])
            ->setAccountMembershipId($data['accountMembershipId'])
            ->setServiceId($data['serviceId'])
            ->setStatus($data['status'])
            ->setCreatedAt($data['createdAt'])
            ->setModifiedAt($data['modifiedAt'])
        ;

        return $booking;
    }

    public function jsonSerialize()
    {
        return [
            'id' => (int) $this->getId(),
            'accountId' => (int) $this->getAccountId(),
            'classId' => (int) $this->getClassId(),
            'locationId' => (int) $this->getLocationId(),
            'siteId' => (int) $this->getSiteId(),
            'membershipServiceId' => (int) $this->getMembershipServiceId(),
            'accountMembershipId' => (int) $this->getAccountMembershipId(),
            'serviceId' => (int) $this->getServiceId(),
            'status' => $this->getStatus(),
        ];
    }
}
