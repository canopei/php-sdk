<?php

namespace Sng\Model\ScheduledClass;

class Program
{
    private $id;
    private $siteId;
    private $mbId;
    private $name;
    private $scheduleType;
    private $cancelOffset;
    private $dropinServiceMbId;
    private $createdAt;
    private $modifiedAt;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Program
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteId()
    {
        return $this->siteId;
    }

    /**
     * @param mixed $siteId
     * @return Program
     */
    public function setSiteId($siteId)
    {
        $this->siteId = $siteId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMbId()
    {
        return $this->mbId;
    }

    /**
     * @param mixed $mbId
     * @return Program
     */
    public function setMbId($mbId)
    {
        $this->mbId = $mbId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Program
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getScheduleType()
    {
        return $this->scheduleType;
    }

    /**
     * @param mixed $scheduleType
     * @return Program
     */
    public function setScheduleType($scheduleType)
    {
        $this->scheduleType = $scheduleType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCancelOffset()
    {
        return $this->cancelOffset;
    }

    /**
     * @param mixed $cancelOffset
     * @return Program
     */
    public function setCancelOffset($cancelOffset)
    {
        $this->cancelOffset = $cancelOffset;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDropinServiceMbId()
    {
        return $this->dropinServiceMbId;
    }

    /**
     * @param mixed $dropinServiceMbId
     * @return Program
     */
    public function setDropinServiceMbId($dropinServiceMbId)
    {
        $this->dropinServiceMbId = $dropinServiceMbId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return Program
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param mixed $modifiedAt
     * @return Program
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
        return $this;
    }

    public static function fromApi($data)
    {
        $program = new self();
        $program
            ->setId($data['programId'])
            ->setSiteId($data['siteId'])
            ->setMbId($data['mbId'])
            ->setName($data['name'])
            ->setScheduleType($data['scheduleType'])
            ->setCancelOffset($data['cancelOffset'])
            ->setDropinServiceMbId($data['dropinServiceMbId'])
            ->setCreatedAt($data['createdAt'])
            ->setModifiedAt($data['modifiedAt'])
        ;

        return $program;
    }
}
