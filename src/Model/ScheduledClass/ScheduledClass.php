<?php

namespace Sng\Model\ScheduledClass;

class ScheduledClass
{
    private $uuid;
    private $mbId;
    private $siteId;
    private $name;
    private $description;
    private $descriptionUuid;
    private $sessionType;
    private $sessionTypeMbId;
    private $programDropinServiceMbId;
    private $level;
    private $maxCapacity;
    private $webCapacity;
    private $totalBooked;
    private $webBooked;
    private $totalBookedWaitlist;
    private $isWaitlistAvailable;
    private $isCanceled;
    private $substitute;
    private $active;
    private $isAvailable;
    private $hideCancel;
    private $classScheduleId;
    private $startDatetime;
    private $endDatetime;
    private $createdAt;
    private $modifiedAt;

    private $staff;
    private $location;

    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function setMbId($mbId)
    {
        $this->mbId = $mbId;

        return $this;
    }

    public function getMbId()
    {
        return $this->mbId;
    }

    public function setSiteId($siteId)
    {
        $this->siteId = $siteId;

        return $this;
    }

    public function getSiteId()
    {
        return $this->siteId;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getDescriptionUuid()
    {
        return $this->descriptionUuid;
    }

    /**
     * @param mixed $descriptionUuid
     */
    public function setDescriptionUuid($descriptionUuid)
    {
        $this->descriptionUuid = $descriptionUuid;
        return $this;
    }

    public function setSessionType($sessionType)
    {
        $this->sessionType = $sessionType;

        return $this;
    }

    public function getSessionType()
    {
        return $this->sessionType;
    }

    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function setMaxCapacity($maxCapacity)
    {
        $this->maxCapacity = $maxCapacity;

        return $this;
    }

    public function getMaxCapacity()
    {
        return $this->maxCapacity;
    }

    public function setWebCapacity($webCapacity)
    {
        $this->webCapacity = $webCapacity;

        return $this;
    }

    public function getWebCapacity()
    {
        return $this->webCapacity;
    }

    public function setTotalBooked($totalBooked)
    {
        $this->totalBooked = $totalBooked;

        return $this;
    }

    public function getTotalBooked()
    {
        return $this->totalBooked;
    }

    public function setWebBooked($webBooked)
    {
        $this->webBooked = $webBooked;

        return $this;
    }

    public function getWebBooked()
    {
        return $this->webBooked;
    }

    public function setTotalBookedWaitlist($totalBookedWaitlist)
    {
        $this->totalBookedWaitlist = $totalBookedWaitlist;

        return $this;
    }

    public function getTotalBookedWaitlist()
    {
        return $this->totalBookedWaitlist;
    }

    public function setIsWaitlistAvailable($isWaitlistAvailable)
    {
        $this->isWaitlistAvailable = $isWaitlistAvailable;

        return $this;
    }

    public function getIsWaitlistAvailable()
    {
        return $this->isWaitlistAvailable;
    }

    public function setIsCanceled($isCanceled)
    {
        $this->isCanceled = $isCanceled;

        return $this;
    }

    public function getIsCanceled()
    {
        return $this->isCanceled;
    }

    public function setSubstitute($substitute)
    {
        $this->substitute = $substitute;

        return $this;
    }

    public function getSubstitute()
    {
        return $this->substitute;
    }

    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setIsAvailable($isAvailable)
    {
        $this->isAvailable = $isAvailable;

        return $this;
    }

    public function getIsAvailable()
    {
        return $this->isAvailable;
    }

    public function setHideCancel($hideCancel)
    {
        $this->hideCancel = $hideCancel;

        return $this;
    }

    public function getHideCancel()
    {
        return $this->hideCancel;
    }

    /**
     * @return mixed
     */
    public function getClassScheduleId()
    {
        return $this->classScheduleId;
    }

    /**
     * @param mixed $classScheduleId
     */
    public function setClassScheduleId($classScheduleId)
    {
        $this->classScheduleId = $classScheduleId;
        return $this;
    }

    public function setStartDatetime($startDatetime)
    {
        $this->startDatetime = $startDatetime;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSessionTypeMbId()
    {
        return $this->sessionTypeMbId;
    }

    /**
     * @param mixed $sessionTypeMbId
     */
    public function setSessionTypeMbId($sessionTypeMbId)
    {
        $this->sessionTypeMbId = $sessionTypeMbId;
        return $this;
    }

    public function getStartDatetime()
    {
        return $this->startDatetime;
    }

    public function setEndDatetime($endDatetime)
    {
        $this->endDatetime = $endDatetime;

        return $this;
    }

    public function getEndDatetime()
    {
        return $this->endDatetime;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProgramDropinServiceMbId()
    {
        return $this->programDropinServiceMbId;
    }

    /**
     * @param mixed $programDropinServiceMbId
     * @return ScheduledClass
     */
    public function setProgramDropinServiceMbId($programDropinServiceMbId)
    {
        $this->programDropinServiceMbId = $programDropinServiceMbId;
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    public function setStaff($staff)
    {
        $this->staff = $staff;

        return $this;
    }

    public function getStaff()
    {
        return $this->staff;
    }

    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function getDurationInSeconds()
    {
        return strtotime($this->endDatetime) - strtotime($this->startDatetime);
    }

    public function getFormattedDuration()
    {
        $secondsDuration = $this->getDurationInSeconds();

        $hours = $secondsDuration / 3600;
        $minutes = ($secondsDuration % 3600) / 60;

        return sprintf('%02d:%02d h', $hours, $minutes);
    }

    public function getStatusForUserSchedule($schedule = [])
    {
        if ($this->getIsCanceled()) {
            return 'Cancelled';
        }

        if ($this->getWebCapacity() > 0 && $this->getWebCapacity() <= $this->getWebBooked()) {
            if ($this->getIsWaitlistAvailable()) {
                return 'Waitlist';
            } else {
                return 'Fully booked';
            }
        }

        if ($this->getMaxCapacity() > 0 && $this->getMaxCapacity() <= $this->getTotalBooked()) {
            if ($this->getIsWaitlistAvailable()) {
                return 'Waitlist';
            } else {
                return 'Fully booked';
            }
        }

        return 'Available';
    }

    public static function fromApi($data)
    {
        $class = new self();
        $class
            ->setUuid($data['uuid'])
            ->setMbId($data['mbId'])
            ->setSiteId($data['siteId'])
            ->setName($data['description']['name'])
            ->setDescriptionUuid($data['description']['uuid'])
            ->setDescription($data['description']['description'])
            ->setSessionType($data['description']['sessionType']['name'])
            ->setSessionTypeMbId($data['description']['sessionType']['mbId'])
            ->setProgramDropinServiceMbId($data['description']['sessionType']['program']['dropinServiceMbId'])
            ->setLevel($data['level']['name'])
            ->setMaxCapacity($data['maxCapacity'])
            ->setWebCapacity($data['webCapacity'])
            ->setTotalBooked($data['totalBooked'])
            ->setWebBooked($data['webBooked'])
            ->setTotalBookedWaitlist($data['totalBookedWaitlist'])
            ->setIsWaitlistAvailable($data['isWaitlistAvailable'])
            ->setIsCanceled($data['isCanceled'])
            ->setSubstitute($data['substitute'])
            ->setActive($data['active'])
            ->setIsAvailable($data['isAvailable'])
            ->setHideCancel($data['hideCancel'])
            ->setClassScheduleId($data['classScheduleId'])
            ->setStartDatetime($data['startDatetime'])
            ->setEndDatetime($data['endDatetime'])
            ->setCreatedAt($data['createdAt'])
            ->setModifiedAt($data['modifiedAt'])
        ;

        return $class;
    }
}
