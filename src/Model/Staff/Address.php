<?php

namespace Sng\Model\Staff;

class Address
{
    private $address;
    private $address2;
    private $city;
    private $state;
    private $country;
    private $postalCode;
    private $foreignZip;

    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    public function getAddress2()
    {
        return $this->address2;
    }

    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    public function getState()
    {
        return $this->state;
    }

    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getPostalCode()
    {
        return $this->postalCode;
    }

    public function setForeignZip($foreignZip)
    {
        $this->foreignZip = $foreignZip;

        return $this;
    }

    public function getForeignZip()
    {
        return $this->foreignZip;
    }

    public static function fromApi($data)
    {
        $address = new self();
        $address
            ->setAddress($data['address'])
            ->setAddress2($data['address2'])
            ->setCity($data['city'])
            ->setState($data['state'])
            ->setCountry($data['country'])
            ->setPostalCode($data['postalCode'])
            ->setForeignZip($data['foreignZip'])
            ;

        return $address;
    }
}
