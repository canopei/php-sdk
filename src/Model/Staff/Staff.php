<?php

namespace Sng\Model\Staff;

class Staff
{
    private $uuid;
    private $mbId;
    private $siteId;
    private $name;
    private $firstName;
    private $lastName;
    private $bio;
    private $gender;
    private $imageUrl;
    private $email;
    private $slug;
    private $phones;
    private $address;
    private $multiLocation;
    private $appointmentTrn;
    private $reservationTrn;
    private $independentContractor;
    private $alwasyAllowDoubleBooking;
    private $userAccessLevel;
    private $featured;
    private $quote;
    private $socialProfiles;
    private $photos;
    private $createdAt;
    private $modifiedAt;

    private $nextClasses;

    public function __construct()
    {
        $this->phones = [];
        $this->socialProfiles = [];
        $this->photos = [];
        $this->nextClasses = [];
    }

    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function setMbId($mbId)
    {
        $this->mbId = $mbId;

        return $this;
    }

    public function getMbId()
    {
        return $this->mbId;
    }

    public function setSiteId($siteId)
    {
        $this->siteId = $siteId;

        return $this;
    }

    public function getSiteId()
    {
        return $this->siteId;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setBio($bio)
    {
        $this->bio = $bio;

        return $this;
    }

    public function getBio()
    {
        return $this->bio;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setPhones(array $phones)
    {
        $this->phones = $phones;

        return $this;
    }

    public function getPhones()
    {
        return $this->phones;
    }

    public function setAddress(Address $address)
    {
        $this->address = $address;

        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setMultiLocation($multiLocation)
    {
        $this->multiLocation = $multiLocation;

        return $this;
    }

    public function getMultiLocation()
    {
        return $this->multiLocation;
    }

    public function setAppointmentTrn($appointmentTrn)
    {
        $this->appointmentTrn = $appointmentTrn;

        return $this;
    }

    public function getAppointmentTrn()
    {
        return $this->appointmentTrn;
    }

    public function setReservationTrn($reservationTrn)
    {
        $this->reservationTrn = $reservationTrn;

        return $this;
    }

    public function getReservationTrn()
    {
        return $this->reservationTrn;
    }

    public function setIndependentContractor($independentContractor)
    {
        $this->independentContractor = $independentContractor;

        return $this;
    }

    public function getIndependentContractor()
    {
        return $this->independentContractor;
    }

    public function setAlwasyAllowDoubleBooking($alwasyAllowDoubleBooking)
    {
        $this->alwasyAllowDoubleBooking = $alwasyAllowDoubleBooking;

        return $this;
    }

    public function getAlwasyAllowDoubleBooking()
    {
        return $this->alwasyAllowDoubleBooking;
    }

    public function setUserAccessLevel($userAccessLevel)
    {
        $this->userAccessLevel = $userAccessLevel;

        return $this;
    }

    public function getUserAccessLevel()
    {
        return $this->userAccessLevel;
    }

    public function setFeatured($featured)
    {
        $this->featured = $featured;

        return $this;
    }

    public function getFeatured()
    {
        return $this->featured;
    }

    public function setQuote($quote)
    {
        $this->quote = $quote;

        return $this;
    }

    public function getQuote()
    {
        return $this->quote;
    }

    public function setSocialProfiles(array $socialProfiles)
    {
        $this->socialProfiles = $socialProfiles;

        return $this;
    }

    public function getSocialProfiles()
    {
        return $this->socialProfiles;
    }

    public function setPhotos(array $photos)
    {
        $this->photos = $photos;

        return $this;
    }

    public function getPhotos()
    {
        return $this->photos;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    public function setNextClasses(array $nextClasses)
    {
        $this->nextClasses = $nextClasses;

        return $this;
    }

    public function getNextClasses()
    {
        return $this->nextClasses;
    }

    public static function fromApi($data)
    {
        $staff = new self();
        $staff
            ->setUuid($data['uuid'])
            ->setMbId($data['mbId'])
            ->setSiteId($data['siteId'])
            ->setName($data['name'])
            ->setFirstName($data['firstName'])
            ->setLastName($data['lastName'])
            ->setBio($data['bio'])
            ->setQuote($data['quote'])
            ->setGender($data['gender'])
            ->setImageUrl($data['imageUrl'])
            ->setEmail($data['email'])
            ->setSlug($data['slug'])
            ->setPhones($data['phones'] ?: [])
            ->setPhotos($data['photos'] ?: [])
            ->setSocialProfiles($data['socialProfiles'] ?: [])
            ->setAddress(Address::fromApi($data['address']))
            ->setMultiLocation($data['multiLocation'])
            ->setAppointmentTrn($data['appointmentTrn'])
            ->setReservationTrn($data['reservationTrn'])
            ->setIndependentContractor($data['independentContractor'])
            ->setAlwasyAllowDoubleBooking($data['alwaysAllowDoubleBooking'])
            ->setUserAccessLevel($data['userAccessLevel'])
            ->setFeatured($data['featured'])
            ->setCreatedAt($data['createdAt'])
            ->setModifiedAt($data['modifiedAt'])
            ;

        return $staff;
    }
}
