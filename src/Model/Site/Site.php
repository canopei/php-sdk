<?php

namespace Sng\Model\Site;

class Site
{
    private $id;
    private $uuid;
    private $name;
    private $description;
    private $slug;
    private $contactEmail;
    private $mbId;
    private $syncedAt;
    private $isApproved;
    private $createdAt;
    private $modifiedAt;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * @param mixed $contactEmail
     */
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMbId()
    {
        return $this->mbId;
    }

    /**
     * @param mixed $mbId
     */
    public function setMbId($mbId)
    {
        $this->mbId = $mbId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSyncedAt()
    {
        return $this->syncedAt;
    }

    /**
     * @param mixed $syncedAt
     */
    public function setSyncedAt($syncedAt)
    {
        $this->syncedAt = $syncedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getisApproved()
    {
        return $this->isApproved;
    }

    /**
     * @param mixed $isApproved
     */
    public function setIsApproved($isApproved)
    {
        $this->isApproved = $isApproved;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param mixed $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
        return $this;
    }

    public static function fromApi($data)
    {
        $site = new self();
        $site
            ->setId($data['id'])
            ->setUuid($data['uuid'])
            ->setName($data['name'])
            ->setDescription($data['description'])
            ->setSlug($data['slug'])
            ->setContactEmail($data['contactEmail'])
            ->setMbId($data['mbId'])
            ->setSyncedAt($data['syncedAt'])
            ->setIsApproved($data['isApproved'])
            ->setCreatedAt($data['createdAt'])
            ->setModifiedAt($data['modifiedAt'])
        ;

        return $site;
    }
}