<?php

namespace Sng\Model\Site;

class City
{
    private $id;
    private $name;
    private $slug;
    private $sortOrder;
    private $imageUrl;
    private $subcities;
    private $longitude;
    private $latitude;
    private $isEmpty;

    public function __construct()
    {
        $this->subcities = [];
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubcities()
    {
        return $this->subcities;
    }

    /**
     * @param mixed $subcities
     */
    public function setSubcities(array $subcities)
    {
        $this->subcities = $subcities;
    }

    /**
     * @return mixed
     */
    public function getIsEmpty()
    {
        return $this->isEmpty;
    }

    /**
     * @param mixed $isEmpty
     */
    public function setIsEmpty($isEmpty)
    {
        $this->isEmpty = $isEmpty;
    }

    public static function fromApi($data)
    {
        $city = new self();
        $city
            ->setId($data['id'])
            ->setName($data['name'])
            ->setSlug($data['slug'])
            ->setImageUrl($data['imageUrl'])
            ->setSortOrder($data['sortOrder'])
            ->setLatitude($data['latitude'])
            ->setLongitude($data['longitude'])
            ->setSubcities($data['subcities'])
            ;

        return $city;
    }
}
