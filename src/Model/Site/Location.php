<?php

namespace Sng\Model\Site;

class Location
{
    private $uuid;
    private $mbId;
    private $siteId;
    private $siteUuid;
    private $name;
    private $description;
    private $slug;
    private $phone;
    private $city;
    private $address;
    private $address2;
    private $postalCode;
    private $hasClasses;
    private $featured;
    private $latitude;
    private $longitude;
    private $createdAt;
    private $modifiedAt;
    private $photos;
    private $openingHours;
    private $lateCancelInterval;

    private $nextClasses;

    const WEEKDAYS = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

    public function __construct()
    {
        $this->photos = [];
        $this->openingHours = [];
        $this->nextClasses = [];
    }

    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function setMbId($mbId)
    {
        $this->mbId = $mbId;

        return $this;
    }

    public function getMbId()
    {
        return $this->mbId;
    }

    public function setSiteId($siteId)
    {
        $this->siteId = $siteId;

        return $this;
    }

    public function getSiteId()
    {
        return $this->siteId;
    }

    /**
     * @return mixed
     */
    public function getSiteUuid()
    {
        return $this->siteUuid;
    }

    /**
     * @param mixed $siteUuid
     */
    public function setSiteUuid($siteUuid)
    {
        $this->siteUuid = $siteUuid;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    public function getAddress2()
    {
        return $this->address2;
    }

    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getPostalCode()
    {
        return $this->postalCode;
    }

    public function setHasClasses($hasClasses)
    {
        $this->hasClasses = $hasClasses;

        return $this;
    }

    public function getHasClasses()
    {
        return $this->hasClasses;
    }

    public function setFeatured($featured)
    {
        $this->featured = $featured;

        return $this;
    }

    public function getFeatured()
    {
        return $this->featured;
    }

    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    public function setPhotos(array $photos)
    {
        $this->photos = $photos;

        return $this;
    }

    public function getPhotos()
    {
        return $this->photos;
    }

    public function setNextClasses(array $nextClasses)
    {
        $this->nextClasses = $nextClasses;

        return $this;
    }

    public function getNextClasses()
    {
        return $this->nextClasses;
    }

    public function setOpeningHours(array $openingHours)
    {
        $this->openingHours = $openingHours;

        return $this;
    }

    public function getOpeningHours()
    {
        return $this->openingHours;
    }

    /**
     * @return mixed
     */
    public function getLateCancelInterval()
    {
        return $this->lateCancelInterval;
    }

    /**
     * @param mixed $lateCancelInterval
     * @return Location
     */
    public function setLateCancelInterval($lateCancelInterval)
    {
        $this->lateCancelInterval = $lateCancelInterval;
        return $this;
    }

    public function getFormattedOpeningHours()
    {
        if (empty($this->openingHours)) {
            return [];
        }

        // Group the hours by weekday, appending and formatting the hours
        $perWeekdayMerged = [];
        foreach ($this->openingHours as $oh) {
            $formattedStartTime = date('g:iA', strtotime($oh['startTime']));
            $formattedEndTime = date('g:iA', strtotime($oh['endTime']));
            $formattedInterval = $formattedStartTime.' - '.$formattedEndTime;

            if (isset($perWeekdayMerged[$oh['weekday']])) {
                $perWeekdayMerged[$oh['weekday']] .= ', '.$formattedInterval;
            } else {
                $perWeekdayMerged[$oh['weekday']] = $formattedInterval;
            }
        }

        // Group the hours by weekdays intervals
        $mergedWeekdays = [];
        foreach ($perWeekdayMerged as $weekday => $hours) {
            foreach ($mergedWeekdays as &$mw) {
                // We group of the hours match
                if ($mw['hours'] == $hours) {
                    // If the weekday is inside the interval or at the edges
                    if ($weekday >= ($mw['startWeekday'] - 1) && $weekday <= ($mw['endWeekday'] + 1)) {
                        // if it's on the edges, extend the edges
                        if ($weekday == $mw['startWeekday'] - 1) {
                            $mw['startWeekday'] -= 1;
                        }
                        if ($weekday == $mw['endWeekday'] + 1) {
                            $mw['endWeekday'] += 1;
                        }

                        // if we matched the day in an interval, we skip to the next weekday.
                        continue 2;
                    }
                }
            }

            // If we didn't match the day in an interval, we create a new entry in the array
            $mergedWeekdays[] = [
                'hours' => $hours,
                'startWeekday' => $weekday,
                'endWeekday' => $weekday,
            ];
        }
        unset($mw);

        // Format the intervals to day strings
        $result = [];
        foreach ($mergedWeekdays as $mw) {
            if ($mw['startWeekday'] == $mw['endWeekday']) {
                $formattedDay = self::WEEKDAYS[$mw['startWeekday']];
            } else {
                $formattedDay = self::WEEKDAYS[$mw['startWeekday']].' - '.self::WEEKDAYS[$mw['endWeekday']];
            }
            $result[$formattedDay] = $mw['hours'];
        }

        return $result;
    }

    public static function fromApi($data)
    {
        $location = new self();
        $location
            ->setUuid($data['uuid'])
            ->setMbId($data['mbId'])
            ->setSiteId($data['siteId'])
            ->setSiteUuid($data['siteUuid'])
            ->setName($data['name'])
            ->setDescription($data['description'])
            ->setSlug($data['slug'])
            ->setPhone($data['phone'])
            ->setCity($data['city'])
            ->setAddress($data['address'])
            ->setAddress2($data['address2'])
            ->setPostalCode($data['postalCode'])
            ->setLateCancelInterval($data['lateCancelInterval'])
            ->setHasClasses($data['hasClasses'])
            ->setFeatured($data['featured'])
            ->setLongitude($data['longitude'])
            ->setLatitude($data['latitude'])
            ->setPhotos($data['photos'] ?: [])
            ->setOpeningHours($data['openingHours'] ?: [])
            ->setCreatedAt($data['createdAt'])
            ->setModifiedAt($data['modifiedAt'])
            ;

        return $location;
    }
}
