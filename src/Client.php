<?php

namespace Sng;

class Client implements ClientInterface
{
    const VERSION = '0.0.1';
    const CONFIG_KEY_APEX = 'apex';

    /**
     * @var \GuzzleHttp\ClientInterface
     */
    private $httpClient;

    /**
     * @var array
     */
    private $config;

    /**
     * @var array
     */
    private $services;

    public function __construct(array $config = array())
    {
        $this->config = array_merge(
            [],
            $config
        );

        if (empty($this->config[self::CONFIG_KEY_APEX])) {
            throw new \Exception('The apex configuration cannot be empty.');
        }

        $this->httpClient = $this->buildHttpClient();
        $this->services = [];
    }

    public function __call($name, $args)
    {
        if (array_key_exists($name, $this->services)) {
            return $this->services[$name];
        }

        $className = 'Sng\\Service\\'.$name;
        if (class_exists($className)) {
            return $this->prepareService($name);
        }

        throw new \BadMethodCallException("Unknown method: {$name}.");
    }

    private function prepareService($serviceName)
    {
        $className = 'Sng\\Service\\'.$serviceName;

        $service = new $className($this->config, $this->httpClient);
        $this->services[$serviceName] = $service;

        return $service;
    }

    private function buildHttpClient()
    {
        $options = [
            'http_errors' => false,
        ];
        if ($proxy = getenv('HTTP_PROXY')) {
            $options['proxy'] = $proxy;
            $options['verify'] = false;
        }

        return new \GuzzleHttp\Client($options);
    }

    public function setHttpClient(\GuzzleHttp\ClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function getHttpClient()
    {
        return $this->httpClient;
    }

    public function getApex()
    {
        return $this->config['apex'];
    }

    public function getVersion()
    {
        return self::VERSION;
    }
}
