<?php

namespace Sng;

interface ClientInterface
{
    public function getApex();
    public function getVersion();
    public function __call($name, $args);
}