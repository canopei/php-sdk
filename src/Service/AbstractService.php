<?php

namespace Sng\Service;

use GuzzleHttp\ClientInterface;

abstract class AbstractService implements ServiceInterface
{
    protected $baseUrl;

    protected $httpClient;

    protected $config;

    protected $authorizationToken;

    public function __construct($config, ClientInterface $httpClient)
    {
        $this->config = $config;
        $this->httpClient = $httpClient;

        $this->baseUrl = static::SERVICE_NAME.'.'.$this->config['apex'];
        if (!is_null(static::SERVICE_VERSION)) {
            $this->baseUrl .= '/'.static::SERVICE_VERSION;
        }
    }

    public function setAuthorizationToken($token)
    {
        $this->authorizationToken = $token;
    }
}
