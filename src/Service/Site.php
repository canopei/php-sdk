<?php

namespace Sng\Service;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Uri;
use Sng\Exception\NotFoundException;
use Sng\Exception\AccessDeniedException;
use Sng\Model\Site\Location;
use Sng\Model\Site\City;

class Site extends AbstractService
{
    const SERVICE_NAME = 'site';
    const SERVICE_VERSION = 'v1';

    const DEFAULT_SITES_COUNT = 15;

    public function getSites($limit = self::DEFAULT_SITES_COUNT, $offset = 0)
    {
        $uri = new Uri($this->baseUrl . '/sites');
        $uri = Uri::withQueryValue($uri, 'limit', $limit);
        $uri = Uri::withQueryValue($uri, 'offset', $offset);

        $request = new Request('GET', $uri);

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return $data;
            default:
                throw new \UnexpectedValueException('Unexpected response status code ' . $statusCode . '.');
        }
    }

    public function getSiteByUuid($uuid)
    {
        $uri = new Uri($this->baseUrl . '/sites/' . $uuid);

        $request = new Request('GET', $uri);

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return \Sng\Model\Site\Site::fromApi($data);
            case 404:
                throw new NotFoundException(sprintf('Site UUID "%s" not found.', $uuid));
            default:
                throw new \UnexpectedValueException('Unexpected response status code ' . $statusCode . '.');
        }
    }

    public function getActivationCode($mbId)
    {
        $uri = new Uri($this->baseUrl . '/sites/' . $mbId . '/activation_codes');

        $request = new Request('GET', $uri);

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return $data;
            case 400:
                throw new \InvalidArgumentException();
            case 404:
                throw new NotFoundException('Site ID ' . $mbId . ' was not found for activation.');
            default:
                throw new \UnexpectedValueException('Unexpected response status code ' . $statusCode . '.');
        }
    }

    public function createSiteFromMb($mbId)
    {
        $uri = new Uri($this->baseUrl . '/sites/mb');

        $request = new Request('POST', $uri, [], json_encode([
            'mbId' => $mbId,
        ]));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return $data;
            case 403:
                throw new AccessDeniedException();
            case 400:
                throw new \InvalidArgumentException();
            case 409:
                throw new AlreadyExistsException();
            default:
                throw new \UnexpectedValueException('Unexpected response status code ' . $statusCode . '.');
        }
    }

    public function queueSyncSiteWithMb(array $uuids)
    {
        $uri = new Uri($this->baseUrl . '/sites/sync');

        $request = new Request('POST', $uri, [], json_encode([
            'uuids' => $uuids,
        ]));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                continue;
            case 400:
                throw new \InvalidArgumentException();
            default:
                throw new \UnexpectedValueException('Unexpected response status code ' . $statusCode . '.');
        }
    }

    public function getLocations(array $uuids)
    {
        if (empty($uuids)) {
            return [];
        }

        $uri = new Uri($this->baseUrl . '/locations/' . implode(',', $uuids));
        $request = new Request('GET', $uri);

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                $locations = [];
                foreach ($data['locations'] as $locationData) {
                    $location = Location::fromApi($locationData);
                    $locations[$location->getUuid()] = $location;
                }

                $ordered = [];
                foreach ($uuids as $uuid) {
                    if (isset($locations[$uuid])) {
                        $ordered[] = $locations[$uuid];
                    }
                }

                return $ordered;
            case 400:
                throw new \InvalidArgumentException();
            default:
                throw new \UnexpectedValueException('Unexpected response status code ' . $statusCode . '.');
        }
    }

    public function getLocationBySlug($slug)
    {
        $uri = new Uri($this->baseUrl . '/locations/slug/' . urlencode($slug));
        $request = new Request('GET', $uri);

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return Location::fromApi($data);
            case 400:
                throw new \InvalidArgumentException();
            case 404:
                throw new NotFoundException(sprintf('Location with slug "%s" not found.', $slug));
            default:
                throw new \UnexpectedValueException('Unexpected response status code ' . $statusCode . '.');
        }
    }

    public function getCities()
    {
        $uri = new Uri($this->baseUrl . '/cities');
        $request = new Request('GET', $uri);

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                $cities = [];
                foreach ($data['cities'] as $cityData) {
                    $cities[] = City::fromApi($cityData);
                }

                return $cities;
            default:
                throw new \UnexpectedValueException('Unexpected response status code ' . $statusCode . '.');
        }
    }

    public function addLocationToFavourites($accountUuid, $locationUuid)
    {
        $uri = new Uri($this->baseUrl . '/locations/' . $locationUuid . '/favourite/' . $accountUuid);

        $request = new Request('POST', $uri, [], json_encode([]));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 204:
                continue;
            default:
                throw new \UnexpectedValueException('Unexpected response status code ' . $statusCode . '.');
        }
    }

    public function removeLocationFromFavourites($accountUuid, $locationUuid)
    {
        $uri = new Uri($this->baseUrl . '/locations/' . $locationUuid . '/favourite/' . $accountUuid);

        $request = new Request('DELETE', $uri, [], json_encode([]));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 204:
                continue;
            default:
                throw new \UnexpectedValueException('Unexpected response status code ' . $statusCode . '.');
        }
    }

    public function getLocationFavouritesForAccount($accountUuid)
    {
        $uri = new Uri($this->baseUrl . '/locations/favourite/' . $accountUuid);
        $request = new Request('GET', $uri);

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return $data;
            default:
                throw new \UnexpectedValueException('Unexpected response status code ' . $statusCode . '.');
        }
    }

    public function getConnectedSites()
    {
        $uri = new Uri($this->baseUrl . '/sites/mb/');
        $request = new Request('GET', $uri);

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                $sites = [];
                foreach ($data as $siteData) {
                    $sites[] = \Sng\Model\Site\Site::fromApi($siteData);
                }

                return $sites;
            default:
                throw new \UnexpectedValueException('Unexpected response status code ' . $statusCode . '.');
        }
    }
}