<?php

namespace Sng\Service;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Uri;
use Sng\Exception\NotFoundException;
use Sng\Model\ScheduledClass\Booking;
use Sng\Model\ScheduledClass\Program;
use Sng\Model\ScheduledClass\ScheduledClass as ScheduledClassModel;
use Sng\Model\Staff\Staff;
use Sng\Model\Site\Location;

class ScheduledClass extends AbstractService
{
    const SERVICE_NAME = 'class';
    const SERVICE_VERSION = 'v1';

    const DEFAULT_STAFF_COUNT = 15;

    public function getClassesByUUIDs(array $uuids, $forceUpdate = false)
    {
        if (empty($uuids)) {
            return [];
        }

        $uri = new Uri($this->baseUrl.'/classes/' . ($forceUpdate ? 'mb/' : '') . implode(',', $uuids));
        $request = new Request('GET', $uri);

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                $classes = [];
                foreach ((array) $data['classes'] as $classData) {
                    $class = ScheduledClassModel::fromApi($classData['class']);
                    $class->setStaff(Staff::fromApi($classData['staff']));
                    $class->setLocation(Location::fromApi($classData['location']));
                    $classes[$class->getUuid()] = $class;
                }

                // We need to preserve the order
                $ordered = [];
                foreach ($uuids as $uuid) {
                    if (isset($classes[$uuid])) {
                        $ordered[] = $classes[$uuid];
                    }
                }

                return $ordered;
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function addAccountToClass($accountUuid, $classUuid, $clientServiceId = null, $requirePayment = true)
    {
        $data = [ 'requirePayment' => $requirePayment ];
        if (!empty($clientServiceId)) {
            $data['clientServiceId'] = (int) $clientServiceId;
        }

        $uri = new Uri($this->baseUrl.'/classes/'.$classUuid.'/account/'.$accountUuid);
        $request = new Request('POST', $uri, [], json_encode($data));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                return;
            case 409:
                throw new \OutOfBoundsException("The client is already booked at this time.");
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function removeAccountFromClass($accountUuid, $classUuid, $lateCancel = false)
    {
        $uri = new Uri($this->baseUrl.'/classes/'.$classUuid.'/account/'.$accountUuid);
        $request = new Request('DELETE', $uri, [], json_encode([
            'lateCancel' => $lateCancel,
        ]));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                return;
            case 404:
                throw new NotFoundException("Class or location not found.");
            case 400:
                throw new \InvalidArgumentException("The acount has no clients on this class' studio.");
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function createBooking($accountUuid, $classUuid, Booking $booking)
    {
        $uri = new Uri($this->baseUrl.'/classes/' . $classUuid . '/account/' . $accountUuid . '/bookings');
        $request = new Request('POST', $uri, [], json_encode($booking));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return Booking::fromApi($data);
            case 400:
                throw new \InvalidArgumentException();
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function updateBooking(Booking $booking)
    {
        $uri = new Uri($this->baseUrl.'/bookings/' . $booking->getId());
        $request = new Request('PUT', $uri, [], json_encode($booking));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return Booking::fromApi($data);
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function getBookingByAccountAndClass($accountUuid, $classUuid)
    {
        $uri = new Uri($this->baseUrl.'/classes/' . $classUuid . '/account/' . $accountUuid . '/bookings');
        $request = new Request('GET', $uri);

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return Booking::fromApi($data);
            case 404:
                throw new NotFoundException("A booking was not found.");
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function getProgramsBySiteUuid($siteUuid)
    {
        $uri = new Uri($this->baseUrl.'/sites/' . $siteUuid . '/programs');
        $request = new Request('GET', $uri);

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                $programs = [];
                foreach ($data['programs'] as $programData) {
                    $programs[] = Program::fromApi($programData);
                }

                return $programs;
            case 404:
                throw new NotFoundException("The site with UUID '" . $siteUuid . "' was not found.");
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }
}
