<?php

namespace Sng\Service;

use GuzzleHttp\Psr7\Request;
use Sng\Exception\AlreadyExistsException;
use Sng\Exception\NotFoundException;
use Sng\Model\Account\Account as AccountModel;
use Sng\Model\Account\AccountMembership;
use Sng\Model\Account\Client;
use Sng\Model\Account\ClientService;
use Sng\Model\Account\InstagramItem;
use Sng\Model\Account\Service;
use Sng\Model\Account\Visit;
use Sng\Model\Sale\Membership;
use \Sng\Model\Site\Site;

class Account extends AbstractService
{
    const SERVICE_NAME = 'account';
    const SERVICE_VERSION = 'v1';

    public function createAccount($name, $email, $password)
    {
        $request = new Request('POST', $this->baseUrl.'/accounts', [], json_encode([
            'email' => $email,
            'password' => $password,
            'fullName' => $name,
        ]));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 409:
                throw new AlreadyExistsException('An account with this e-mail already exists.');
            case 400:
                throw new \InvalidArgumentException();
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return AccountModel::fromApi($data);
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function createAccountWithExternalLogin(
        $externalProviderName,
        $externalUserId,
        $email,
        $firstName,
        $lastName,
        $profileImageUrl
    ) {
        $request = new Request('POST', $this->baseUrl.'/accounts/social', [], json_encode([
            'externalProviderName' => $externalProviderName,
            'externalUserId' => $externalUserId,
            'email' => $email,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'profileImageUrl' => $profileImageUrl,
        ]));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 409:
                throw new AlreadyExistsException('An account with this e-mail already exists.');
            case 400:
                throw new \InvalidArgumentException();
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return AccountModel::fromApi($data);
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function getAccount($search)
    {
        $request = new Request('GET', $this->baseUrl.'/accounts/'.urlencode($search));
        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 404:
                throw new NotFoundException('Could not find an account.');
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return AccountModel::fromApi($data);
            case 500:
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function getAccountByExternalUserID($providerName, $externalUserId)
    {
        $request = new Request('GET', $this->baseUrl.'/accounts/external/'.urlencode($providerName).'/'.urlencode($externalUserId));
        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 404:
                throw new NotFoundException('Could not find the account.');
            case 400:
                throw new \InvalidArgumentException();
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return AccountModel::fromApi($data);
            case 500:
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function confirmAccount($uuid, $code)
    {
        $request = new Request('POST', $this->baseUrl.'/accounts/'.urlencode($uuid).'/confirmation/'.urlencode($code), [], json_encode(new \stdClass()));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 409:
                throw new AlreadyExistsException('The account is already confirmed.');
            case 404:
                throw new NotFoundException('The account was not found.');
            case 400:
                throw new \InvalidArgumentException();
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return AccountModel::fromApi($data);
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function changePassword($uuid, $newPassword, $currentPassword = '')
    {
        $requestData = [
            'newPassword' => $newPassword,
        ];

        if (!empty($currentPassword)) {
            $requestData['currentPassword'] = $currentPassword;
        }
        $request = new Request('POST', $this->baseUrl.'/accounts/'.urlencode($uuid).'/password', [], json_encode($requestData));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 404:
                throw new NotFoundException('The account was not found.');
            case 400:
                throw new \InvalidArgumentException();
            case 204:
                return;
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function createPasswordChangeRequest($uuid)
    {
        $request = new Request('POST', $this->baseUrl.'/accounts/'.urlencode($uuid).'/password_change_requests', [], json_encode(new \stdClass()));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 404:
                throw new NotFoundException('The account was not found.');
            case 400:
                throw new \InvalidArgumentException();
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return $data;
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function getAccountByPasswordChangeRequestCode($code)
    {
        $request = new Request('GET', $this->baseUrl.'/accounts/password_change_requests/'.urlencode($code));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 404:
                throw new NotFoundException('The account was not found or invalid code.');
            case 400:
                throw new \InvalidArgumentException();
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return AccountModel::fromApi($data);
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function getClientsByEmail($email, $siteId = 0)
    {
        if (empty($email)) {
            return [];
        }

        $request = new Request('GET', $this->baseUrl.'/clients/search/'.urlencode($email).'?siteId=' . (int) $siteId);

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                $clients = [];
                foreach ((array) $data['clients'] as $clientData) {
                    $client = Client::fromApi($clientData['client']);
                    $client->setSite(Site::fromApi($clientData['site']));
                    $clients[$client->getUuid()] = $client;
                }

                return $clients;
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function getClientsByAccount($accountUuid)
    {
        if (empty($accountUuid)) {
            return [];
        }

        $request = new Request('GET', $this->baseUrl.'/accounts/'.$accountUuid.'/clients');

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                $clients = [];
                foreach ((array) $data['clients'] as $clientData) {
                    $client = Client::fromApi($clientData['client']);
                    $client->setSite(Site::fromApi($clientData['site']));
                    $clients[$client->getUuid()] = $client;
                }

                return $clients;
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function linkClientToAccount($accountUuid, $clientUuid) {
        $request = new Request('POST', $this->baseUrl.'/accounts/'.$accountUuid.'/clients/'.$clientUuid, [], json_encode(new \stdClass()));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 204:
                break;
            case 409:
                throw new AlreadyExistsException('A link already exists for these account and client.');
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function unlinkClientFromAccount($accountUuid, $clientUuid) {
        $request = new Request('DELETE', $this->baseUrl.'/accounts/'.$accountUuid.'/clients/'.$clientUuid, [], json_encode(new \stdClass()));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 204:
                break;
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function getInstagramItemsForUsername($username, $limit = 10)
    {
        if (empty($username)) {
            return [];
        }

        $request = new Request('GET', $this->baseUrl.'/accounts/'.$username.'/instagram/media?limit=' . (int) $limit);

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                $items = [];
                foreach ((array) $data['items'] as $itemData) {
                    $items[] = InstagramItem::fromApi($itemData);
                }

                return $items;
            case 404:
                throw new NotFoundException('Instagram account ' . $username . ' was not found.');
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function getClientServicesForClass($clientUUID, $classUUID)
    {
        if (empty($clientUUID)) {
            return [];
        }

        $request = new Request('GET', $this->baseUrl.'/clients/'.$clientUUID.'/services/class/' . $classUUID);

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                $items = [];
                foreach ((array) $data['services'] as $itemData) {
                    $items[] = ClientService::fromApi($itemData);
                }

                return $items;
            case 404:
                throw new NotFoundException('Client was not found.');
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function getAccountVisitsHistory($accountUuid, $limit = 15, $offset = 0)
    {
        if (empty($accountUuid)) {
            return [];
        }

        $request = new Request('GET', $this->baseUrl.'/accounts/'.$accountUuid.'/visits?limit='.$limit.'&offset='.$offset);

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                $items = [];
                foreach ((array) $data['items'] as $itemData) {
                    $items[] = Visit::fromApi($itemData);
                }

                return $items;
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function getAccountServices($accountUuid, $activeOnly = false)
    {
        if (empty($accountUuid)) {
            return [];
        }

        $request = new Request('GET', $this->baseUrl.'/accounts/'.$accountUuid.'/services'.($activeOnly ? '?showActiveOnly=1' : ''));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                $items = [];
                foreach ((array) $data['items'] as $itemData) {
                    $items[] = Service::fromApi($itemData);
                }

                return $items;
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function createAccountMembership($accountUuid, $membershipUuid)
    {
        $request = new Request('POST', $this->baseUrl.'/accounts/'.$accountUuid.'/memberships/'.$membershipUuid, [], json_encode(new \stdClass()));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                $item = AccountMembership::fromApi($data);
                if (isset($data['membership'])) {
                    $item->setMembership(Membership::fromApi($data['membership']));
                }

                return $item;
            case 404:
                throw new AlreadyExistsException('A link already exists for these account and client.');
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function getAccountMemberships($accountUuid)
    {
        if (empty($accountUuid)) {
            return [];
        }

        $request = new Request('GET', $this->baseUrl.'/accounts/'.$accountUuid.'/memberships');

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                $items = [];
                foreach ((array) $data['items'] as $itemData) {
                    $item = AccountMembership::fromApi($itemData);
                    if (isset($itemData['membership'])) {
                        $item->setMembership(Membership::fromApi($itemData['membership']));
                    }

                    $items[] = $item;
                }

                return $items;
            case 404:
                throw new NotFoundException("Account " . $accountUuid . " not found.");
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function updateAccountMembership(AccountMembership $accountMembership)
    {
        $request = new Request('PUT', $this->baseUrl.'/accounts/memberships/' . $accountMembership->getId(), [], json_encode($accountMembership));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return AccountMembership::fromApi($data);
            case 404:
                throw new NotFoundException("Account membership ID " . $accountMembership->getId() . " not found.");
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function getAccountSchedule($accountUuid, $endDate = null)
    {
        if (empty($accountUuid)) {
            return [];
        }

        if (empty($endDate)) {
            $endDate = date('Y-01-01 00:00:00', strtotime('+1 year'));
        }

        $request = new Request('GET', $this->baseUrl.'/accounts/'.$accountUuid.'/schedule?endDate='.urlencode($endDate));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                $items = [];
                foreach ((array) $data['items'] as $itemData) {
                    $items[] = Visit::fromApi($itemData);
                }

                return $items;
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function createClient($siteMbId, Client $client)
    {
        $request = new Request('POST', $this->baseUrl.'/clients', [], json_encode([
            'siteMbId' => $siteMbId,
            'client' => $client,
        ]));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return Client::fromApi($data);
            case 404:
                throw new NotFoundException('Client was not found.');
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function expireAccountService($siteUuid, $serviceId)
    {
        $request = new Request('PUT', $this->baseUrl.'/accounts/services/' . $serviceId . '/expire', [], json_encode([
            'siteUuid' => $siteUuid
        ]));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 204:
                return;
            case 400:
                throw new \InvalidArgumentException();
            case 404:
                throw new NotFoundException('Site was not found.');
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }
}
