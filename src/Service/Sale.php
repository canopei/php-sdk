<?php

namespace Sng\Service;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Uri;
use Sng\Exception\NotFoundException;
use Sng\Model\Sale\Membership;
use Sng\Model\Sale\Purchase;
use Sng\Model\Sale\Service;

class Sale extends AbstractService
{
    const SERVICE_NAME = 'sale';
    const SERVICE_VERSION = 'v1';

    public function getSaleServicesForSite($siteUuid)
    {
        $uri = new Uri($this->baseUrl . '/sites/' . $siteUuid . '/services');

        $request = new Request('GET', $uri);

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                $services = [];
                foreach ($data['services'] as $serviceData) {
                    $services[] = Service::fromApi($serviceData);
                }

                return $services;
            case 404:
                throw new NotFoundException();
            default:
                throw new \UnexpectedValueException('Unexpected response status code ' . $statusCode . '.');
        }
    }

    public function getSaleServicesForClass($classUuid)
    {
        $uri = new Uri($this->baseUrl . '/classes/' . $classUuid . '/services');

        $request = new Request('GET', $uri);

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                $services = [];
                foreach ($data['services'] as $serviceData) {
                    $services[] = Service::fromApi($serviceData);
                }

                return $services;
            case 404:
                throw new NotFoundException();
            default:
                throw new \UnexpectedValueException('Unexpected response status code ' . $statusCode . '.');
        }
    }

    public function createSimpleSale($accountUuid, $siteUuid, $serviceMbId, $classUuid = '')
    {
        $uri = new Uri($this->baseUrl . '/sites/' . $siteUuid . '/sales');

        $request = new Request('POST', $uri, [], json_encode([
            'accountUuid' => $accountUuid,
            'classUuid' => $classUuid,
            'items' => [
                [
                    'mbId' => (int) $serviceMbId,
                    'quantity' => 1,
                ]
            ]
        ]));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return $data;
            case 400:
                throw new \InvalidArgumentException();
            case 404:
                throw new NotFoundException();
            default:
                throw new \UnexpectedValueException('Unexpected response status code ' . $statusCode . '.');
        }
    }

    public function getAvailableMemberships(array $uuids = [])
    {
        $uri = new Uri($this->baseUrl . '/memberships/' . implode(',', $uuids));

        $request = new Request('GET', $uri);

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                $memberships = [];
                foreach ($data['memberships'] as $membershipData) {
                    $memb = Membership::fromApi($membershipData);

                    if (!$memb->getIsAvailable()) {
                        continue;
                    }

                    $memberships[] = $memb;
                }

                return $memberships;
            default:
                throw new \UnexpectedValueException('Unexpected response status code ' . $statusCode . '.');
        }
    }

    public function createPurchase($accountUuid, $purchase)
    {
        $uri = new Uri($this->baseUrl . '/purchases');

        $data = json_decode(json_encode($purchase), true);
        unset($data['accountId']);
        $data['accountUuid'] = $accountUuid;

        $request = new Request('POST', $uri, [], json_encode($data));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return Purchase::fromApi($data);
            case 400:
                throw new \InvalidArgumentException();
            case 404:
                throw new NotFoundException("Account not found.");
            default:
                throw new \UnexpectedValueException('Unexpected response status code ' . $statusCode . '.');
        }
    }
}