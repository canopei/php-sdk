<?php

namespace Sng\Service;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Uri;
use Sng\Model\Staff\Staff as StaffModel;

class Staff extends AbstractService
{
    const SERVICE_NAME = 'staff';
    const SERVICE_VERSION = 'v1';

    const DEFAULT_STAFF_COUNT = 15;

    public function getStaffBySlug($slug)
    {
        $uri = new Uri($this->baseUrl.'/staff/slug/'.urlencode($slug));
        $request = new Request('GET', $uri);

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return StaffModel::fromApi($data);
            case 400:
                throw new \InvalidArgumentException();
            case 404:
                throw new NotFoundException(sprintf('Staff with slug "%s" not found.', $slug));
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function getStaffByUUIDs(array $uuids)
    {
        $uri = new Uri($this->baseUrl.'/staff/'.implode(',', $uuids));
        $request = new Request('GET', $uri);

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                $staff = [];
                foreach ((array) $data['staff'] as $staffData) {
                    $s = StaffModel::fromApi($staffData);
                    $staff[$s->getUuid()] = $s;
                }

                // We need to preserve the order
                $ordered = [];
                foreach ($uuids as $uuid) {
                    if (isset($staff[$uuid])) {
                        $ordered[] = $staff[$uuid];
                    }
                }

                return $ordered;
            default:
                throw new \UnexpectedValueException('Unexpected response status code '.$statusCode.'.');
        }
    }

    public function addStaffToFavourites($accountUuid, $staffUuid)
    {
        $uri = new Uri($this->baseUrl . '/staff/' . $staffUuid . '/favourite/' . $accountUuid);

        $request = new Request('POST', $uri, [], json_encode([]));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 204:
                continue;
            default:
                throw new \UnexpectedValueException('Unexpected response status code ' . $statusCode . '.');
        }
    }

    public function removeStaffFromFavourites($accountUuid, $staffUuid)
    {
        $uri = new Uri($this->baseUrl . '/staff/' . $staffUuid . '/favourite/' . $accountUuid);

        $request = new Request('DELETE', $uri, [], json_encode([]));

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 204:
                continue;
            default:
                throw new \UnexpectedValueException('Unexpected response status code ' . $statusCode . '.');
        }
    }

    public function getStaffFavouritesForAccount($accountUuid)
    {
        $uri = new Uri($this->baseUrl . '/staff/favourite/' . $accountUuid);
        $request = new Request('GET', $uri);

        $response = $this->httpClient->send($request, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();

        switch ($statusCode) {
            case 200:
                $data = json_decode($response->getBody(), true);
                if (is_null($data)) {
                    throw new \UnexpectedValueException('Could not JSON decode the response.');
                }

                return $data;
            default:
                throw new \UnexpectedValueException('Unexpected response status code ' . $statusCode . '.');
        }
    }
}
